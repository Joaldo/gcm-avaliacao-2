package br.ucsal.bes20211.gcm.avaliacao2;

import java.util.Scanner;

public class Calculo {

	private Scanner scanner = new Scanner(System.in);

	// Método que será testado pela classe CalculoTest.
	public int calcularSoma() {
		int num1;
		int num2;

		// Entrada de dados 1.
		System.out.println("Informe o primeiro número:");
		num1 = scanner.nextInt();

		// Entrada de dados 2.
		System.out.println("Informe o segundo número:");
		num2 = scanner.nextInt();

		return num1 + num2;
	}

	// Método que será testado pela classe CalculoTest.
	public void apresentarResultado(int x) {
		String mensagem = "O resultado da operação foi:" + x;
		System.out.println(mensagem);
	}

}
