package br.ucsal.bes20211.gcm.avaliacao2;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculoOutTest {

	@Test
	void testarApresentarResultado46() throws InterruptedException {
		
		Thread.sleep(60000);
		
		Calculo calculo = new Calculo();

		int x = 46;

		final String LINE_SEPARTOR = System.lineSeparator();

		String saidaEsperada = "O resultado da operação foi:46" + LINE_SEPARTOR;

		ByteArrayOutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));

		calculo.apresentarResultado(x);
		// Como faço pra obter o resultado atual???

		String saidaAtual = outFake.toString();

		Assertions.assertEquals(saidaEsperada, saidaAtual);

	}
}
